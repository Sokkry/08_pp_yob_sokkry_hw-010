import React from 'react'
import { Button, Nav, Navbar, Form, FormControl, Container } from 'react-bootstrap'
import { Link} from "react-router-dom";

export default function Menubar() {

    return (
        <Container>
            <Navbar bg="light" expand="lg">
                <Navbar.Brand as={Link} to="/">AMS Management</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        <Nav.Link as={Link} to="/">Home</Nav.Link>
                        <Nav.Link as={Link} to="/">Post</Nav.Link>
                        <Nav.Link as={Link} to="/">User</Nav.Link>
                        <Nav.Link as={Link} to="/category">Category</Nav.Link>
                        <Nav.Link as={Link} to="/author">Author</Nav.Link>
                    </Nav>
                    <Form inline>
                        <FormControl type="text" placeholder="Search" className="mr-sm-2" />
                        <Button variant="outline-success">Search</Button>
                    </Form>
                </Navbar.Collapse>
            </Navbar>
        </Container>
    )
}
