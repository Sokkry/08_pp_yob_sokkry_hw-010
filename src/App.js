
import './App.css';
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Menubar from './components/Menubar';
import Author from './views/Author';

function App() {
  return (
    <BrowserRouter>
      <Menubar/>
      <Switch>
        <Route path="/author" component={Author} />
      </Switch>
    </BrowserRouter>

  );
}

export default App;
