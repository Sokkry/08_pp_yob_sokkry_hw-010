import axios from "axios";
//Base URL
export const API = axios.create({
  baseURL: "http://110.74.194.124:3034/api",
});

//fetch all Author
export const fetchAllArticles = async () => {
  try {
    const result = await API.get("/author");
    console.log("fetchAllArticles:", result.data.data);
    return result.data.data;
  } catch (error) {
    console.log("fetchAllArticles Error:", error);
  }
};

export const fetchArticleById = async(id)=>{
  try {
    const result  = await API.get(`/author/${id}`)
    console.log("fetchArticleById:", result.data.data);
    return result.data.data
  } catch (error) {
    console.log("fetchArticleById error:", error);
  }
}


//delete Author
export const deleteAritcleById = async(id) => {
    try {
        const result = await API.delete(`/author/${id}`);
        console.log("deleteAritcleById:", result.data.data);
        return result.data.data;
      } catch (error) {
        console.log("deleteAritcleById Error:", error);
      }
}

//upload images
export const uploadImage = async(image) => {
   try {
    const fd = new FormData();
    fd.append("image", image)
    const result = await API.post("/images", fd, {
        onUploadProgress: progress=>{
            console.log("Uploaded:", Math.floor(progress.loaded / progress.total * 100));
        }
    })
    console.log("uploadImage:", result.data.url);
    return result.data.url
   } catch (error) {
       console.log("uploadImage Error:", error);
   }
}


//Add Author
export const addArticle = async(article) => {
    try {
        const result = await API.post("/author", article);
        console.log("addArticle:", result.data.data);
        return result.data.data
      } catch (error) {
        console.log("addArticle Error:", error);
      }
}

//update Author
export const updateArticleById = async(id, article)=>{
  try {
    const result = await API.patch(`/author/${id}`, article)
    console.log("updateArticleById:", result.data.data);
    return result.data.data
  } catch (error) {
    console.log("updateArticleById Error:", error);
  }
}
