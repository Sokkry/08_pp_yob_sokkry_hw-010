import React, { useEffect, useState } from "react";
import { Table, Button, Container,Row,Col,Form } from "react-bootstrap";
// import Moment from "react-moment";
import { useHistory,useLocation } from "react-router";
import { 
    deleteAritcleById, 
    fetchAllArticles,addArticle,
    fetchArticleById,
    uploadImage,
    updateArticleById,
 } from "../services/Services";
 import query from 'query-string'

export default function Author() {
    const [articles, setArticles] = useState([]);
    const history = useHistory();
    const [date, setDate] = useState(Date.now())

    useEffect(async () => {
        const result = await fetchAllArticles();
        console.log("Articles:", result);
        setArticles(result);
    }, []);

    //Delete article by ID
    async function onDeleteArticleById(id) {
        const result = await deleteAritcleById(id);
        const temp = articles.filter(item => {
            return item._id != id
        })
        setArticles(temp)
    }

    //// Add Author
    const [myImage, setMyImage] = useState(null);
    const [browsedImage, setBrowsedImage] = useState("");
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    
    const placeholder = "https://cdn4.iconfinder.com/data/icons/communication-280/32/communication_35-512.png";
  
    const {search} = useLocation()
    console.log("Location:", search);
  
    let {id} = query.parse(search)
    console.log("id query:", id);
  
    useEffect(async() => {
      if(search == ""){
        setName("")
        setEmail("")
        setBrowsedImage("")
      }else{
        const result = await fetchArticleById(id)
      setName(result.name)
      setEmail(result.email)
      setBrowsedImage(result.image)
      }
  
    }, [id]);
  
  
    //Browse image  
    function onBrowsedImage(e) {
      setMyImage(e.target.files[0]);
      setBrowsedImage(URL.createObjectURL(e.target.files[0]));
    }
  
    //Add and Update Author
    async function onAddArticle() {
      if(search == ""){
        const url = myImage && await uploadImage(myImage);
        const article = {
          name,
          email,
          image: url ? url : placeholder,
        };
        const result = await addArticle(article);
      }else{
        
        const url = myImage && await uploadImage(myImage);
        
        const article = {
          name,
          email,
          image: url ? url : browsedImage,
        };
        const result = await updateArticleById(id, article)
      }
        
    }
  
   
    return (
        <Container>
    {/* //// Add Author and update Author */}
            <Row className="my-5">
                
                <Col md={8}>
                    <h1>Author</h1>
                    <Form>
                        {/* <h2>{search ? "Update" : "Add"} Author</h2> */}
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Author Name</Form.Label>
                            <Form.Control
                                type="text"
                                value={name}
                                onChange={(e) => setName(e.target.value)}
                                placeholder="Author Name"
                            />
                            <Form.Text className="text-muted">
                            </Form.Text>
                        </Form.Group>

                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Email</Form.Label>
                            <Form.Control
                                type="text"
                                value={email}
                                onChange={(e) => setEmail(e.target.value)}
                                placeholder="Email"
                            />
                            <Form.Text className="text-muted">
                            </Form.Text>
                        </Form.Group>

                        <Button
                            onClick={onAddArticle}
                            className="my-3"
                            variant="primary"
                            type="button"
                        >
                            {search ? "Update" : "Add"}
                        </Button>
                    </Form>
                </Col>
                <Col md={4}>
                    <div style={{ width: "300px" }}>
                        <label htmlFor="myfile">
                            <img width="100%" src={browsedImage ? browsedImage : placeholder} />
                        </label>
                    </div>
                    <p>Choose image</p>
                    <input
                        onChange={onBrowsedImage}
                        id="myfile"
                        type="file"
                        style={{ display: "" }}
                    />
                </Col>
            </Row>


{/* Show table author */}
            <Table striped bordered hover>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Image</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    {articles.map((item, index) => {
                        return (
                            <tr key={index}>
                                <td>{item._id.slice(0, 8)}</td>
                                <td>{item.name}</td>
                                <td>{item.email}</td>
                                <td>
                                    <img width="200px" src={item.image} />
                                </td>
                                <td>
                                    <Button onClick={() => history.push(`/author?id=${item._id}`)} variant="warning">Edit</Button>{' '}
                                    <Button onClick={() => onDeleteArticleById(item._id)} variant="danger">Delete</Button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </Table>
        </Container>
    );
}

const headerStyle = {
    color: 'white',
    backgroundColor: 'green',
}